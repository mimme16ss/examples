/* eslint-env node */
"use strict";
var myModule = (function() {
  var that = {},
    http = require("http");

  function getArguments() {
    var args = {
      url: process.argv[2],
    };
    return args;
  }

  function getRequestOptionsFrom(url) {
    var protocol = url.split("//")[0],
      port = (protocol === "https:") ? 443 : 80,
      hostname = url.split("//")[1].split("/")[0],
      path = url.split(hostname)[1] || "/";
    return {
      hostname: hostname,
      port: port,
      path: path,
      method: "GET",
      agent: false,
    };
  }

  function onWebsiteAvialable(data) {
    console.log(data);
  }

  function requestWebsite(url, callback) {
    var options = getRequestOptionsFrom(url);
    http.get(options, function(res) {
      var data = "";
      res.on("data", function(partialData) {
        data += partialData;
      });
      res.on("end", function() {
        callback(data);
      });
    });
  }

  function run() {
    var args = getArguments();
    if (args.url) {
      console.log("Starting parsing process");
      requestWebsite(args.url, onWebsiteAvialable);
    } else {
      console.log("No URL passed");
    }
  }

  that.run = run;
  return that;
}());

myModule.run();
