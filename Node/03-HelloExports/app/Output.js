/* eslint-env node */
"use strict";
var Output = function() {
  var that = {};

  function log(msg) {
    console.log(msg);
  }

  that.log = log;
  return that;
};

module.exports = Output;
