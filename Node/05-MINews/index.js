/* eslint-env node */
"use strict";

// Run `npm install jsdom` to install all dependencies for this demo
var newsScraper = (function() {
  var that = {},
    Server = require("./app/Server.js"),
    Connector = require("./app/Connector.js"),
    Scraper = require("./app/Scraper.js"),
    newsServer,
    args;

  function getArguments() {
    var args = {
      url: process.argv[2],
      port: parseInt(process.argv[3], 10),
    };
    if (!Number.isInteger(args.port)) {
      args.port = undefined;
    }
    return args;
  }

  function onNewsScraped(news) {
    newsServer.setNews(news);
  }

  function onWebsiteAvailable(html) {
    var scraper = new Scraper();
    scraper.scrapeNews(html, onNewsScraped);
  }

  function update(url) {
    var connector = new Connector();
    connector.requestWebsite(url, onWebsiteAvailable);
  }

  function startServer(port) {
    newsServer = new Server();
    newsServer.start(port);
  }

  function run() {
    args = getArguments();
    if (args.port) {
      startServer(args.port);
    } else {
      console.log("No PORT passed");
      process.exit(1);
    }
    if (args.url) {
      update(args.url, onWebsiteAvailable);
    } else {
      console.log("No URL passed");
      process.exit(1);
    }
  }

  that.run = run;
  return that;
}());

newsScraper.run();
