/* eslint-env node */
"use strict";
var Connector = function() {
  var that = {},
    jsdom = require("jsdom");

  function getNewsText(parts) {
    var index, text = "";
    for (index = 0; index < parts.length; index++) {
      text += parts[index].innerHTML;
    }
    return text;
  }

  function parseSingleNewsArticle(article) {
    var teaserUrl = article.querySelector("img"),
      title = article.querySelector("h2"),
      date = article.querySelector(".kicker"),
      text = getNewsText(article.querySelectorAll("p"));
    if (teaserUrl) {
      teaserUrl = teaserUrl.src;
    }
    if (title) {
      title = title.innerHTML.trim();
    }
    if (date) {
      date = date.innerHTML.trim();
    }
    return {
      teaser: teaserUrl,
      title: title,
      date: date,
      text: text,
    };
  }

  function getNewsFromDocument(document, callback) {
    var index, news, allNews = [],
      articles = document.querySelectorAll(".article");
    for (index = 0; index < articles.length; index++) {
      news = parseSingleNewsArticle(articles[index]);
      if (news) {
        allNews.push(news);
      }
    }
    callback(allNews);
  }

  function onDomEnviromentAvailable(callback, err, window) {
    var document = window.document;
    getNewsFromDocument(document, callback);
  }

  function scrapeNews(html, callback) {
    jsdom.env(html, onDomEnviromentAvailable.bind(this, callback));
  }

  that.scrapeNews = scrapeNews;
  return that;
};

module.exports = Connector;
