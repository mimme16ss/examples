/* eslint-env node */
"use strict";
var Server = function() {
  var that = {},
    http = require("http"),
    server,
    routes,
    currentNews;

  function setNews(news) {
    currentNews = news;
  }

  function start(port) {
    server = http.createServer(onRequest);
    addRoute("/news.json", serveNews);
    server.listen(port);
  }

  function serveNews(response) {
    var news = JSON.stringify(currentNews);
    response.end(news);
  }

  function serverError(response, error) {
    response.end(error);
  }

  function checkRoutes(request, response) {
    var route;
    if (!routes) {
      serverError(response, "404");
    } else {
      for (route in routes) {
        if (routes.hasOwnProperty(route)) {
          if (route === request.url) {
            routes[route](response);
            return;
          }
        }
      }
      serverError(response, "404");
    }
  }

  function addRoute(route, callback) {
    if (!routes) {
      routes = {};
    }
    if (routes[route]) {
      return;
    }
    routes[route] = callback;
  }

  function onRequest(request, response) {
    checkRoutes(request, response);
  }

  that.setNews = setNews;
  that.start = start;
  return that;
};

module.exports = Server;
