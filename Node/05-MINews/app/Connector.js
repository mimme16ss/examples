/* eslint-env node */
"use strict";
var Connector = function() {
  var that = {},
    http = require("http"),
    Defaults = {
      HTTP_PORT: 80,
      HTTPS_PORT: 443,
    };

  function getRequestOptionsFrom(url) {
    var protocol = url.split("//")[0],
      port = (protocol === "https:") ? Defaults.HTTPS_PORT : Defaults.HTTP_PORT,
      hostname = url.split("//")[1].split("/")[0],
      path = url.split(hostname)[1] || "/";
    return {
      hostname: hostname,
      port: port,
      path: path,
      method: "GET",
      agent: false,
    };
  }

  function requestWebsite(url, callback) {
    var options = getRequestOptionsFrom(url);
    http.get(options, function(res) {
      var data = "";
      res.on("data", function(partialData) {
        data += partialData;
      });
      res.on("end", function() {
        callback(data);
      });
    });
  }

  that.requestWebsite = requestWebsite;
  return that;
};

module.exports = Connector;
