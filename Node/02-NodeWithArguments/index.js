/* eslint-env node */
"use strict";
var myModule = (function() {
  var that = {};

  function checkArguments() {
    var index, args = process.argv;
    for (index = 0; index < args.length; index++) {
      console.log(args[index]);
    }
  }

  function run() {
    console.log("Using node with arguments");
    checkArguments();
  }

  that.run = run;
  return that;
}());

myModule.run();
