var Time = {};

Time.SECOND = {
    asMilliSeconds: function() {
        return 1000;
    }
};

Time.MINUTE = {
    asMilliSeconds: function() {
        return 60 * Time.SECOND.asMilliSeconds();
    }
};

Time.HOUR = {
    asMilliSeconds: function() {
        return 60 * Time.MINUTE.asMilliSeconds();
    }
};
