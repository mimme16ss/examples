/* global Time */

var WorldClock = {};

WorldClock.UTC_TIMEZONE = {
    newYork: -4,
    london: 0,
    moskau: 3,
    peking: 8,
    tokyo: 9
};

WorldClock.DAYLIGHT_SAVING = {
    london: {
        start: new Date(2016, 2, 27, 1, 0),
        end: new Date(2016, 9, 30, 1, 0)
    }
};

WorldClock.init = function() {
    var clocks, clock, city, index;
    WorldClock.update();
    clocks = document.querySelectorAll(".clock");

    for(index = 0; index < clocks.length; index++) {
        clock = clocks[index];
        city = clock.getAttribute("class").split(" ")[1];
        clock.addEventListener("click", WorldClock.setTime.bind(this, city));
    }

    document.querySelector(".button.refresh").addEventListener("click", WorldClock.update);
};

WorldClock.update = function() {
    WorldClock.setTime("newYork");
    WorldClock.setTime("london");
    WorldClock.setTime("regensburg");
    WorldClock.setTime("moskau");
    WorldClock.setTime("peking");
    WorldClock.setTime("tokyo");
};

WorldClock.setTime = function(city) {
    var hoursHand = document.querySelector("." + city + " .hand.hours"),
        minutesHand = document.querySelector("." + city + " .hand.minutes"),
        time = WorldClock.getTime(city);

    hoursHand.style.transform = "rotate(" + time.hoursInDegreesOnClock + "deg)";
    minutesHand.style.transform = "rotate(" + time.minutesInDegressOnClock + "deg)";
};

WorldClock.getTime = function(city) {
    var date = new Date(),
        utcOffset,
        localOffset,
        hours,
        minutes;

    if (city !== "regensburg") {
        utcOffset = date.getTimezoneOffset() * Time.MINUTE.asMilliSeconds();
        localOffset = WorldClock.UTC_TIMEZONE[city] * Time.HOUR.asMilliSeconds();
        if (WorldClock.DAYLIGHT_SAVING[city] !== undefined) {
            if (date.getTime() > WorldClock.DAYLIGHT_SAVING[city].start.getTime() && date.getTime() < WorldClock.DAYLIGHT_SAVING[city].end.getTime()) {
                localOffset = localOffset + Time.HOUR.asMilliSeconds();
            }
        }
        date = new Date(date.getTime() + utcOffset + localOffset);
    }

    hours = date.getHours() % 12;
    minutes = date.getMinutes();

    if (hours === 0) {
        hours = 12;
    }

    return {
        hours: hours,
        minutes: minutes,
        hoursInDegreesOnClock: hours * (360 / 12),
        minutesInDegressOnClock: minutes * (360 / 60)
    };
};
