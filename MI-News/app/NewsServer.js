/* eslint-env node */
"use strict";
var express = require("express"),
  xmlbuilder = require("xmlbuilder"),
  NewsServer = function() {
    var that = {},
      DEFAULTS = {
        RESPONSE_FORMAT: "json",
      },
      api,
      currentNews;

    function setNews(news) {
      currentNews = news;
    }

    function filterNews(before, after, id) {
      var start = after || new Date("1970-1-1").getTime(),
        end = before || Date.now(),
        targetId,
        result = [],
        index;
      for (index = 0; index < currentNews.length; index++) {
        targetId = id || currentNews[index].id;
        if (currentNews[index].date > start && currentNews[index].date < end && currentNews[index].id === targetId) {
          result.push(currentNews[index]);
        }
      }
      return result;
    }

    function getNewsAs(format, before, after, id) {
      var news = filterNews(before, after, id),
        result;
      switch (format) {
        case "json":
          result = JSON.stringify(news);
          break;
        case "xml":
          result = xmlbuilder.create({ root: { news: news, }, });
          result = result.end({ pretty: true, });
          break;
        default:
          break;
      }
      return result;
    }

    function getContentType(format) {
      switch (format) {
        case "json":
          return "application/json";
        case "xml":
          return "application/xml";
        default:
          return "application/json";
      }
    }

    function onAllNewsRequested(req, res) {
      var format = req.query.format || DEFAULTS.RESPONSE_FORMAT,
        news = getNewsAs(format, req.query.before, req.query.after),
        responseType = getContentType(format);
      res.set("Content-Type", responseType);
      if (news) {
        res.send(news);
      } else {
        res.status(422).send("Invalid query parameters");
      }
    }

    function onNewsRequested(req, res) {
      var format = req.query.format || DEFAULTS.RESPONSE_FORMAT,
        news = getNewsAs(format, req.query.before, req.query.after, req.params.id),
        responseType = getContentType(format);
      res.set("Content-Type", responseType);
      if (news) {
        res.send(news);
      } else {
        res.status(422).send("Invalid query parameters");
      }
    }

    function start(port) {
      var resultPromise;
      api = express();
      api.get("/news", onAllNewsRequested);
      api.get("/news/:id", onNewsRequested);
      resultPromise = new Promise(function(resolve, reject) {
        api.listen(port, function() {
          resolve();
        });
      });

      return resultPromise;
    }

    that.setNews = setNews;
    that.start = start;
    return that;
  };

module.exports = NewsServer;
