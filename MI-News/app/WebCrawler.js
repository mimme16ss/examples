/* eslint-env node */
"use strict";
var http = require("http"),
  WebCrawler = function() {
    var that = {},
      HttpCodes = {
        OK: 200,
      },
      Defaults = {
        HTTP_PORT: 80,
        HTTPS_PORT: 443,
      };

    function getRequestOptionsFrom(url) {
      var protocol = url.split("//")[0],
        port = (protocol === "https:") ? Defaults.HTTPS_PORT : Defaults.HTTP_PORT,
        hostname = url.split("//")[1].split("/")[0],
        path = url.split(hostname)[1] || "/";
      return {
        hostname: hostname,
        port: port,
        path: path,
        method: "GET",
        agent: false,
      };
    }

    function getWebsite(url) {
      var options = getRequestOptionsFrom(url),
        resultPromise = new Promise(function(resolve, reject) {
          http.get(options, function(res) {
            var data = "";
            if (res.statusCode !== HttpCodes.OK) {
              reject();
            }
            res.on("data", function(partialData) {
              data += partialData;
            });
            res.on("end", function() {
              resolve(data);
            });
          });
        });
      return resultPromise;
    }

    that.getWebsite = getWebsite;
    return that;
  };

module.exports = WebCrawler;
