/* eslint-env node */
"use strict";
var CliUtilities = function() {
  var that = {};

  function getOptions(source, target) {
    var index, argument;
    for (index = 0; index < source.length - 1; index++) {
      argument = source[index];
      if (argument.indexOf("--") === -1) {
        continue;
      }
      argument = argument.replace("--", "");
      if (target.hasOwnProperty(argument)) {
        target[argument] = source[index + 1];
      }
    }
    return target;
  }

  that.getOptions = getOptions;
  return that;
};

module.exports = CliUtilities;
