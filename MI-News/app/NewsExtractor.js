/* eslint-env node */
"use strict";
var jsdom = require("jsdom"),
  md5 = require("md5"),
  NewsExtractor = function() {
    var that = {};

    function getNewsText(parts) {
      var index, text = "";
      for (index = 0; index < parts.length; index++) {
        text += parts[index].innerHTML;
      }
      return text;
    }

    function parseSingleNewsArticle(article) {
      var teaserUrl = article.querySelector("img"),
        title = article.querySelector("h2"),
        date = article.querySelector(".kicker"),
        text = getNewsText(article.querySelectorAll("p")),
        id;

      if (teaserUrl) {
        teaserUrl = teaserUrl.src;
      }
      if (title) {
        title = title.innerHTML.trim();
      }
      if (date) {
        date = date.innerHTML.trim();
        date = date.split(".");
        if (date.length !== 3) {
          date = undefined;
        } else {
          date = new Date(date[2] + "-" + date[1] + "-" + date[0]).getTime();
        }
      }
      if (title && date) {
        id = md5(title + date);
      }
      return {
        id: id,
        teaser: teaserUrl,
        title: title,
        date: date,
        text: text,
      };
    }

    function getNewsFromDocument(document) {
      var index, news, allNews = [],
        articles = document.querySelectorAll(".article");
      for (index = 0; index < articles.length; index++) {
        news = parseSingleNewsArticle(articles[index]);
        if (news) {
          allNews.push(news);
        }
      }
      return allNews;
    }

    function getNews(html) {
      var resultPromise = new Promise(function(resolve, reject) {
        jsdom.env(html, function(err, window) {
          var news;
          if (err) {
            reject();
          }
          news = getNewsFromDocument(window.document);
          resolve(news);
        });
      });
      return resultPromise;
    }

    that.getNews = getNews;
    return that;
  };

module.exports = NewsExtractor;
