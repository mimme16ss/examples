/* eslint-env node */
"use strict";

var newsScraper = (function() {
  var that = {},
    cli = require("./app/CliUtilities.js")(),
    NewsServer = require("./app/NewsServer.js"),
    WebCrawler = require("./app/WebCrawler.js"),
    NewsExtractor = require("./app/NewsExtractor.js"),
    newsServer;

  function onError(error) {
    console.log(error);  // eslint-disable-line no-console
    process.exit(1);
  }

  function onNewsScraped(news) {
    newsServer.setNews(news);
  }

  function onWebsiteAvailable(html) {
    var extractor = new NewsExtractor();
    extractor.getNews(html).then(onNewsScraped).catch(onError.bind(this, "Error while extracting news"));
  }

  function update(url) {
    var crawler = new WebCrawler();
    crawler.getWebsite(url).then(onWebsiteAvailable).catch(onError.bind(this, "Error while fetching HTML"));
  }

  function startServer(args) {
    newsServer = new NewsServer();
    newsServer.start(args.port).then(update.bind(this, args.url));
  }

  function run() {
    var args = cli.getOptions(process.argv, { url: undefined, port: undefined, });
    if (args.port && args.url) {
      startServer(args);
    } else {
      onError("Missing parameters");
    }
  }

  that.run = run;
  return that;
}());

newsScraper.run();
