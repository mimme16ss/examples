var MyMensa = (function() {
    "use strict";
    /* eslint-env browser  */

    var that = {},
        model,
        view;

    function init() {
        model = new MyMensa.MensaModel({
            baseUrl: "http://132.199.139.24:9001/mensa/uni/",
            weekdays: ["mo", "di", "mi", "do", "fr"],
            maxTitleLength: 30,
            titleSuffix: "...",
            localStorageKey: "MyMensaFavourites"
        }).init();
        model.load();

        view = new MyMensa.MensaViewController({
            data: model.getDataProxy(),
            templates: {
                dayContainer: document.querySelector("#day-container").innerHTML,
                dayEntry: document.querySelector("#day-entry").innerHTML
            },
            favButtonSelector: ".fav-icon i",
            view: document.querySelector("#week")
        }).init();

        model.addOnChangeListener(onModelChanged);
        model.update();
    }

    function onModelChanged() {
        model.save();
        view.update();
    }

    that.init = init;
    return that;
}());
