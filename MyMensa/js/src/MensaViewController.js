var MyMensa = MyMensa || {};
MyMensa.MensaViewController = function(options) {
    "use strict";
    /* eslint-env browser  */
    /* global _ */

    var that = {},
        createDayContainer,
        createDayEntry;

    function init() {
        createDayContainer = _.template(options.templates.dayContainer);
        createDayEntry = _.template(options.templates.dayEntry);
        return that;
    }

    function update() {
        clear();
        render();
    }

    function clear() {
        while (options.view.firstChild) {
            options.view.removeChild(options.view.firstChild);
        }
    }

    function render() {
        renderDay(options.data.getMenuForMonday(), "Montag");
        renderDay(options.data.getMenuForThuesday(), "Dienstag");
        renderDay(options.data.getMenuForWednesday(), "Mittwoch");
        renderDay(options.data.getMenuForThursday(), "Donnerstag");
        renderDay(options.data.getMenuForFriday(), "Freitag");
    }

    function renderDay(day, label) {
        var container, entry;
        container = bindObjectTo({ name: label }, createDayContainer);
        _.each(day, function(item) {
            entry = bindObjectTo(item, createDayEntry);
            entry.querySelector(options.favButtonSelector).addEventListener("click", item.toggleFavouriteStatus);
            container.appendChild(entry);
        });
        options.view.appendChild(container);
    }

    function bindObjectTo(object, renderFunction) {
        var tmp = document.createElement("div");
        tmp.innerHTML = renderFunction(object);
        return tmp.children[0];
    }

    that.init = init;
    that.update = update;
    return that;
};
