var MyMensa = MyMensa || {};
MyMensa.MensaModel = function(options) {
    "use strict";
    /* eslint-env browser  */
    /* global _, qwest */

    var that = {},
        data,
        favourites,
        labels,
        onChangeListeners,
        daysProcessed;

    function init() {
        favourites = [];
        data = [];
        onChangeListeners = [];
        labels = {
            S: "Suppe",
            H: "Hauptgericht",
            B: "Beilage",
            N: "Dessert",
            getLabel: function(category) {
                return labels[category.charAt(0)];
            }
        };
        return that;
    }

    function notifyAll() {
        _.each(onChangeListeners, function(callback) {
            callback();
        });
    }

    function addOnChangeListener(callback) {
        onChangeListeners.push(callback);
    }

    function getDataProxy() {
        var proxy = {};
        proxy.getMenuForMonday = getMenuForDay.bind(this, "mo");
        proxy.getMenuForThuesday = getMenuForDay.bind(this, "di");
        proxy.getMenuForWednesday = getMenuForDay.bind(this, "mi");
        proxy.getMenuForThursday = getMenuForDay.bind(this, "do");
        proxy.getMenuForFriday = getMenuForDay.bind(this, "fr");
        return proxy;
    }

    function getMenuForDay(day) {
        return _.filter(data, function(item) {
            return item.day.toLowerCase() === day.toLowerCase();
        });
    }

    function getFavourtieStatusForItem(itemID) {
        return _.contains(favourites, itemID);
    }


    function loadFromStorage() {
        var tmp = localStorage.getItem(options.localStorageKey);
        if (tmp) {
            favourites = JSON.parse(tmp) || [];
        }
    }

    function saveToStorage() {
        var tmp = JSON.stringify(favourites) || [];
        localStorage.setItem(options.localStorageKey, tmp);
    }

    function onDataAvailable(newData) {
        data = data.concat(processData(newData));
        daysProcessed++;
        if (daysProcessed === options.weekdays.length) {
            notifyAll();
        }
    }

    function update() {
        data = [];
        daysProcessed = 0;
        _.each(options.weekdays, function(day) {
            qwest.get(options.baseUrl + day)
                .then(function(xhr, response) {
                    onDataAvailable(JSON.parse(response));
                });
        });
    }

    function onFavouriteStatusChanged(itemID) {
        var index = favourites.indexOf(itemID);
        if (index !== -1) {
            favourites.splice(index, 1);
        } else {
            favourites.push(itemID);
        }
        notifyAll();
    }

    function processData(newData) {
        return _.map(newData, function(item) {
            item.category = labels.getLabel(item.category);
            item.score = item.upvotes - item.downvotes;
            item.toggleFavouriteStatus = onFavouriteStatusChanged.bind(this, item.id);
            item.isFavourite = getFavourtieStatusForItem.bind(this, item.id);
            if (item.name.length > options.maxTitleLength) {
                item.shortName = item.name.substring(0, options.maxTitleLength) + options.titleSuffix;
            } else {
                item.shortName = item.name;
            }
            delete item.upvotes;
            delete item.downvotes;
            delete item.labels;
            delete item.costs;
            return item;
        });
    }

    that.init = init;
    that.update = update;
    that.save = saveToStorage;
    that.load = loadFromStorage;
    that.getDataProxy = getDataProxy;
    that.addOnChangeListener = addOnChangeListener;
    return that;
};
