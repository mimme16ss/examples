/* eslint no-unused-vars: 0 */
var LocalTime = (function() {
    var SECOND = {
            asMilliSeconds: function() {
                return 1000;
            }
        },
        MINUTE = {
            asMilliSeconds: function() {
                return 60 * SECOND.asMilliSeconds();
            }
        },
        HOUR = {
            asMilliSeconds: function() {
                return 60 * MINUTE.asMilliSeconds();
            }
        },
        CITIES = [{
            name: "New York",
            utc: -4,
            dts: undefined
        }, {
            name: "London",
            utc: 0,
            dts: {
                start: new Date(2016, 2, 27, 1, 0),
                end: new Date(2016, 9, 30, 1, 0)
            }
        }, {
            name: "Regensburg",
            utc: 1,
            dts: {
                start: new Date(2016, 2, 27, 1, 0),
                end: new Date(2016, 9, 30, 1, 0)
            }
        }, {
            name: "Moskau",
            utc: 3,
            dts: undefined
        }, {
            name: "Peking",
            utc: 8,
            dts: undefined
        }, {
            name: "Tokyo",
            utc: 9,
            dts: undefined
        }];

    function getCityByName(name) {
        var index, searchString, queryString = name.replace(/ /g, "").toLowerCase();
        for (index = 0; index < CITIES.length; index++) {
            searchString = CITIES[index].name.replace(/ /g, "").toLowerCase();
            if (searchString === queryString) {
                return CITIES[index];
            }
        }
        return undefined;
    }

    function LocalTime(location) {
        var time, utcOffset, localOffset, city = getCityByName(location);
        if (location !== undefined) {
            time = new Date();
            utcOffset = time.getTimezoneOffset() * MINUTE.asMilliSeconds();
            localOffset = city.utc * HOUR.asMilliSeconds();
            if (city.dts !== undefined) {
                if (time.getTime() > city.dts.start.getTime() && time.getTime() < city.dts.end.getTime()) {
                    localOffset = localOffset + HOUR.asMilliSeconds();
                }
            }
            time = new Date(time.getTime() + utcOffset + localOffset);
        }
        return time;
    }

    return LocalTime;
}());
