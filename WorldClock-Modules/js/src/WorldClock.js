var WorldClock = (function() {
    var that = {},
        UPDATE_INTERVAL_IN_MS = 60000,
        model,
        view,
        updateTicker;

    function init() {
        view = new WorldClock.View();
        model = new WorldClock.Model(view);
        model.addCity("New York", ".newYork");
        model.addCity("London", ".london");
        model.addCity("Regensburg", ".regensburg");
        model.addCity("Regensburg", ".regensburg");
        model.addCity("Moskau", ".moskau");
        model.addCity("Peking", ".peking");
        model.addCity("Tokyo", ".tokyo");
        model.update();
    }

    function start() {
        updateTicker = setInterval(model.update, UPDATE_INTERVAL_IN_MS);
    }

    function stop() {
        clearInterval(updateTicker);
    }

    that.init = init;
    that.start = start;
    that.stop = stop;
    return that;
}());
