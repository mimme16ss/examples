/* global LocalTime */
var WorldClock = WorldClock || {};
WorldClock.Model = function(newView) {
    var that = {},
        cities = [],
        view = newView;

    function update() {
        var index, city;
        for (index = 0; index < cities.length; index++) {
            city = cities[index];
            updateTimeForCity(city);
        }
    }

    function updateTimeForCity(city) {
        var hours, minutes, date = LocalTime(city);
        hours = date.getHours() % 12;
        minutes = date.getMinutes();
        if (hours === 0) {
            hours = 12;
        }
        view.setClock(city, hours, minutes);
    }

    function addCity(name, selector) {
        cities.push(name);
        view.addClock(name, selector);
    }

    that.update = update;
    that.addCity = addCity;
    return that;
};