var WorldClock = WorldClock || {};
WorldClock.View = function() {
    var that = {},
        clocks = [];

    function getClockById(id) {
        var index;
        for (index = 0; index < clocks.length; index++) {
            if (clocks[index].id === id) {
                return clocks[index];
            }
        }
        return undefined;
    }

    function addClock(id, selector) {
        var node = document.querySelector(selector);
        clocks.push({
            id: id,
            node: node
        });
    }

    function setClock(id, hours, minutes) {
        var hoursDeg, minutesDeg, clock = getClockById(id);
        if (clock !== undefined) {
            hoursDeg = (360 / 12) * hours;
            minutesDeg = (360 / 60) * minutes;
            clock.node.querySelector(".hand.hours").style.transform = "rotate(" + hoursDeg + "deg)";
            clock.node.querySelector(".hand.minutes").style.transform = "rotate(" + minutesDeg + "deg)";
        }
    }

    that.addClock = addClock;
    that.setClock = setClock;
    return that;
};